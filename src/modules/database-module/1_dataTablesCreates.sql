DROP DATABASE IF EXISTS test;
CREATE DATABASE test;
USE test;

DROP TABLE IF EXISTS product;
CREATE TABLE product (
  -- specific  fields
  id              INT               NOT NULL    AUTO_INCREMENT,
  name            TEXT              NOT NULL,
  description     TEXT              NOT NULL,
  
  -- keys
  PRIMARY KEY (id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;






