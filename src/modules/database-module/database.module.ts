import { Module } from '@nestjs/common';
import { TypeOrmModule} from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { InventarioModule } from  '../inventario/inventario.module';
import { ProductEntity } from '../../entity/product.entity';

@Module({
	imports:[
		TypeOrmModule.forRoot({
			type: 'mysql', 
			host:'localhost',
			port:3306,
			username:'root', 
			password: '1234', 
			database:'test',
			entities:[
				ProductEntity
			 ],
			 synchronize:true
		}),
		InventarioModule,
		
	]
	
})
export class DatabaseModule {
	constructor( private readonly connection:Connection){
	}
}
